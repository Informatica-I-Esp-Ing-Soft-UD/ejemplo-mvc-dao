# Ejemplo MVC-DAO

Práctica para aplicar los patrones MVC y DAO en Python

## ¿Cómo probarlo?
Se deberá ejecutar el módulo ```run```, el cual internamente está accediendo a un módulo con una función para prueba en consola. Para hacerlo usamos en consola la instrucción:

```
py -m run 
```

Allí, seguír el menú.
![Prueba del módulo en consola](/ilustraciones/prueba.png)